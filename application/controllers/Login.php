<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @name Login
 * Description: Login controller
 * @author David Bryan Padilla A.
 */
class Login extends CI_Controller {

    /**
     * Description: Index Page for this controller.
     * @name index
     * @return  Load login view
     */
    public function index() {
        $this->load->view('login_view');
    }

    /**
     * validateUser
     * @param Array user form with cipher values 
     * @return json object with status if process was successful, object with error statuses otherwise
     */
    public function validateUser() {
        log_message('info', 'REQUEST: Login Controller');
        if ($_POST['credentials']) {
            $this->load->library('utils/Utilities');
            $this->load->library('form_validation');
            $encrypted = base64_decode($_POST['credentials']['encrypted']);
            $key = base64_decode($_POST['credentials']['key']);
            $iv = base64_decode($_POST['credentials']['iv']);
            $decrypted = Utilities::aes128_cbc_decryptClient($key, $encrypted, $iv);
            $decrypted = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $decrypted);
            $decrypted = json_decode($decrypted);
            if($validateFileds = $this->validateFields()){
                log_message('info', 'PARAMETROS->'.  json_encode($decrypted));
                
            }else{
                log_message("error", "ENTRO AL ELSE DE VALIDA");
            }
        } else {
            redirect("/");
        }
    }
     /**
     * Validate Fields 
     * @param NA
     * @return Bool It's depends of the response
     */
    private function validateFields() {
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '<p>');
        $this->form_validation->set_rules('userName', '', 'trim|min_length[4]|max_length[20]|xss_clean');
        $this->form_validation->set_rules('pass', '', 'trim|min_length[4]|max_length[20]|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            return true;
        }
    }

}
