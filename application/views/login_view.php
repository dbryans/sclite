<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Description: Administration login view
 * Date: 2017/Ago/09
 * Author: dbryan
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>School Cloud Lite </title>

        <!-- Bootstrap -->
        <link href="<?php echo site_url('libs/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo site_url('libs/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
        <!-- NProgress -->
        <link href="<?php echo site_url('libs/nprogress/nprogress.css') ?>" rel="stylesheet">
        <!-- Animate.css -->
        <link href="<?php echo site_url('libs/animate.css/animate.min.css') ?>" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="<?php echo site_url('build/css/custom.min.css') ?>" rel="stylesheet">
        <!-- jQuery -->
        <script src="<?php echo site_url('libs/jquery/dist/jquery.min.js') ?>"></script>
        <!-- Bootstrap -->
        <script src="<?php echo site_url('libs/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
        <!-- FastClick -->
        <script src="<?php echo site_url('libs/fastclick/lib/fastclick.js') ?>"></script>
        <!-- NProgress -->
        <script src="<?php echo site_url('libs//nprogress/nprogress.js') ?>"></script>
        <!-- CryptoJS312 -->
        <script src="<?php echo site_url("js/CryptoJS312/rollups/aes.js") ?>"></script>
        <script src="<?php echo site_url("js/CryptoJS312/rollups/pbkdf2.js") ?>"></script>
        <!-- Custom Theme Scripts -->
        <script src="<?php echo site_url('build/js/custom.min.js') ?>"></script>
        <!-- Underscore -->
        <script src="<?php echo site_url('js/libs/underscore.js') ?>"></script>
        <!-- Backbone -->
        <script src="<?php echo site_url('js/libs/backbone.min.js') ?>"></script>
        <!-- Login -->
        <script src="<?php echo site_url('js/login.js') ?>"></script>
    <body class="login">
        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>

            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        <form class="form-horizontal form-label-left" onsubmit="return false;">
                            <h1>Bienvenido</h1>
                            <div class="col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" name="userName" id="userName" placeholder="Usuario" maxlength="20">
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" name="pass" id="pass" placeholder="Contraseña" maxlength="20">
                                <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div>
                                <button id="send" type="submit" class="btn btn-primary" > Ingresar </button>
                                <!--<a class="reset_pass" href="#">Lost your password?</a>-->
                            </div>

                            <div class="clearfix"></div>

                            <div class="separator">
                                <p class="change_link">¿Perdiste tu contraseña?
                                    <a href="#signup" class="to_register"> Reestablecer contraseña </a>
                                </p>

                                <div class="clearfix"></div>
                                <br />

                                <div>
                                    <h1><i class="fa fa-cloud"></i> School Cloud Lite</h1>
                                    <p>©2017 Todos los Derechos Reservados.</p>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>

                <div id="register" class="animate form registration_form">
                    <section class="login_content">
                        <form>
                            <h1>Reestablecer</h1>
                            <div>
                                <input type="text" class="form-control" placeholder="Nombre de usuario" required="" />
                            </div>
                            <div>
                                <input type="email" class="form-control" placeholder="Email" required="" />
                            </div>

                            <div>
                                   <button id="send" type="submit" class="btn btn-primary" > Enviar </button>
                            </div>

                            <div class="clearfix"></div>

                            <div class="separator">
                                <p class="change_link">
                                    <a href="#signin" class="to_register"> Regresar a p&aacute;gina de acceso </a>
                                </p>

                                <div class="clearfix"></div>
                                <br />

                                <div>
                                    <h1><i class="fa fa-cloud"></i> School Cloud Lite</h1>
                                    <p>©2017 Todos los Derechos Reservados.</p>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </body>
</html>


