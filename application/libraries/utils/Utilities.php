<?php

if (!defined('BASEPATH'))
    exit('No se permite el acceso directo al script');

//============================================================+
// File name   : Utilities.php
//
// Description : Class to manage general application utilities
//
// Author: David Bryan Padilla A.
//
// (c) Copyright: Metáfora Acatlán
// 
// Date: 02/Mar/2017
//============================================================+
/**
 * Description of login
 *
 * @author dbryans
 */
class Utilities {

    /**
     * @param String $key the key to decrypt
     * @param String $data the data to decript (base64 decoded from api response)
     * @param String $iv the initial vector
     * @return String
     */
    static function aes128_cbc_decryptClient($key, $data, $iv) {
        $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);
        return $data;
    }

    /**
     * @param int $currentSession session value from Data Base
     * @param int $session session value from Login
     * @return bool True if session values are the same, False in other case
     */
    static function sameSession($currentSession, $session){
        if($currentSession == $session){
            log_message("info", "Session IGUAL");
            return true;
        }else {
            log_message("info", "Session DIFERENTE");
            return false;
        }
    }
}
