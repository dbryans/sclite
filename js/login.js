TIME_ANIMATION = 400;
ERROR = 'parsley-error';
MIN_SIZE = 5;
MAX_SIZE = 15;

function CipherData() {
    this.iv = CryptoJS.enc.Hex.parse('1017A1C131B15161018191F1b1c1d1F1c');
    this.key = CryptoJS.PBKDF2('ooFuaD0NUth7ebie' + Math.random().toString(36).slice(2), CryptoJS.lib.WordArray.random(128 / 8), {keySize: 256 / 32, iterations: 500});
    var _this = this;

    function encrypt(message) {
        var encrypted = CryptoJS.AES.encrypt(message, _this.key, {iv: _this.iv});
        var data_base64 = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
        var iv_base64 = encrypted.iv.toString(CryptoJS.enc.Base64);
        var key_base64 = encrypted.key.toString(CryptoJS.enc.Base64);
        return {encrypted: data_base64, key: key_base64, iv: iv_base64};
    }

    function sha1(message) {
        return CryptoJS.SHA1(message, {outputLength: 512}).toString(CryptoJS.enc.Hex);
    }

    return {
        encrypt: function (message) {
            return encrypt(message);
        }
        , sha1: function (message) {
            return sha1(message);
        }
    };
}
function RegExprValidator() {

    this.hasDelay = false;
    this.VALIDATE = {
        USER: 'userName'
        , PASSWORD: 'pass'
    };
    this[this.VALIDATE.USER] = {
        functions: ['validateRestrictedText', 'validateEmptyText']
    };
    this[this.VALIDATE.PASSWORD] = {
        functions: ['validateUserPassword', 'validateEmptyText']
    };
   

    this.validateRestrictedText = function (relatedString, $element) {
        var regExpr = /^([a-zA-Z0-9]+)|([a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)$/;
        if (relatedString.length === 0) {
            return {isValid: new RegExp(regExpr).test(relatedString), message: 'Empty'};
        } else {
            return {isValid: new RegExp(regExpr).test(relatedString), message: 'incorrectCredentials'};
        }
    };
    this.validateUserPassword = function (relatedString, $element) {
        var regExpr = /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ\.-_@]*$/;
        if (relatedString.length === 0) {
            return {isValid: new RegExp(regExpr).test(relatedString), message: 'Empty'};
        } else {
            return {isValid: new RegExp(regExpr).test(relatedString), message: 'incorrectCredentials'};
        }
    };

    this.validateEmptyText = function (relatedString, $element) {
        try {
            if (relatedString.length <= 0) {
                return {isValid: false, message: 'Empty'};
            }
        } catch (e) {
            return {isValid: false, message: 'Empty'};
        }
        return {isValid: true, message: ''};
    };

    function validateInputs($element, character) {
        var functions = _this[$element.attr('name')]['functions'];
        var relatedString = $element.val().trim(), dataType = $element.attr('data-validation');
        var response = null;
        try {
            for (var i = 0; i < functions.length; i++) {
                if (character) {
                    response = _this[functions[i]](character, $element);
                } else {
                    response = _this[functions[i]](relatedString, $element, character);
                }
                response.type = functions[i];
                if (!response.isValid) {
                    return response;
                }
            }
        } catch (e) {
            console.log(e);
        }
        return {isValid: true, message: ''};
    }

    function validateRules($element, character) {
        return validateInputs($element, character);
    }
    var _this = this;
    return {
        validate: function ($element) {
            _this.hasDelay = false;
            return validateRules($element);
        }
    };
}
function getLocation() {
    
    return window.location.href;
    
}
var LoginModel = Backbone.Model.extend({
    defaults: {
        url: getLocation() + 'login/validateUser',
        credentials: {user: '', password: ''},
        crypto: new CipherData()
    }
    , initialize: function () {
    }
    , autenticate: function (user, password) {
        this.attributes.credentials.user = user;
        this.attributes.credentials.password = password;
        var credentials = this.attributes.crypto.encrypt(JSON.stringify(this.attributes.credentials));
        var _this = this;
        console.log(credentials);
        console.log(window.location.href);
        return $.ajax({
            url: _this.attributes.url,
            type: "POST",
            data: {selection: 'logIn', credentials: credentials}
        });
    }
});
var Login = Backbone.View.extend({
    el: 'body',
    defaults: {
        elementUser: 'input[name="userName"]'
        , elementPassword: 'input[name="pass"]'
        , elementLogIn: '.buttonlogin'
//        , elementCaptcha: '#g-recaptcha-response'
//        , userContainer: '#userContainer'
//        , passContainer: '#passContainer'
//        , captchaContainer: 'div.error'
//        , alertError: 'div.alert'
//        elementLoader: '.wrapperLoader',
//        modalWindow:'.loginContainerPopUp',
        , error: '.error'
        , validator: new RegExprValidator()
    }
    , events: {
        'click #send': 'logIn'
      //  , 'change #password': 'logIn'
        , 'keypress .form-control': 'nextField'
        , 'submit .form-control': 'clearAlerts'
    }
    ,initialize: function(){
        console.log("vista login funciona");
    }
    , nextField: function (event) {
        var _this = this;
        var $target = $(event.currentTarget);
        if ($target.val().length > MIN_SIZE) {
            $(this.el).find("#" + $target.attr("name")).removeClass(ERROR);
        } else {
            $(this.el).find("#" + $target.attr("name")).addClass(ERROR);
        }
    }
    , logIn: function () {
        var _this = this;
        var $user = $(this.defaults.elementUser);
        var $password = $(this.defaults.elementPassword);
//        var $elementCaptcha = $(this.defaults.elementCaptcha);
        var responseUser = this.defaults.validator.validate($user);
        var responsePassword = this.defaults.validator.validate($password);
//        var responseCaptcha = this.defaults.validator.validate($elementCaptcha);

        if (!responseUser.isValid) {
            $user.addClass(ERROR);
            return false;
        }
        if (!responsePassword.isValid) {
            $password.addClass(ERROR);
            return false;
        }
        
        
//        if (!responseCaptcha.isValid) {
//            $(this.defaults.captchaContainer).slideDown(TIME_ANIMATION);
//            return false;
//        } else {
//            $(this.defaults.captchaContainer).slideUp(TIME_ANIMATION);
//        }
//        $(".bg-loader , .loader").slideDown(TIME_ANIMATION);

        this.model.autenticate($user.val(), $password.val()).done(function (response) {
            if (response) {
                if (response.success) {
                    window.location = window.location.protocol + "//" + window.location.host + "/sclite/dashboard";
                }
                if (response.error) {
                    $(".form-control").val("");
//                    grecaptcha.reset();
//                    $(".bg-loader , .loader").slideUp(TIME_ANIMATION);
                    $(_this.defaults.alertError).slideDown(TIME_ANIMATION).find("#message").text(response.message);
                }
            } else {
                $(".form-control").val("");
//                grecaptcha.reset();
//                $(".bg-loader , .loader").slideUp(TIME_ANIMATION);
                $(_this.defaults.alertError).slideDown(TIME_ANIMATION).find("#message").text("Ocurrio un problema intenta de nuevo.");
            }

        });
    }
    , clearAlerts: function (event) {
        var _this = this;
        $(_this.defaults.alertError).slideUp(TIME_ANIMATION);
    }
});

$(document).ready(function () {
    cipherData = new CipherData();
    login = new Login({model: new LoginModel()});
});



